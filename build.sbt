name := "titanic"

organization := "nyaapa"

version := "0.0.1"

scalaVersion := "2.10.4"

resolvers ++= Seq(
  "IESL Release" at "http://dev-iesl.cs.umass.edu/nexus/content/groups/public",
  "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"
)

libraryDependencies ++= Seq(
  "com.github.tototoshi" %% "scala-csv" % "1.2.0",
  "org.apache.spark" %% "spark-core" % "1.2.0",
  "org.apache.spark" %% "spark-mllib" % "1.2.0" % "provided"
)

xerial.sbt.Pack.packAutoSettings
