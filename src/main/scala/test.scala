/**
 * Created by nyaapa on 2/28/15.
 */

import java.io.File

import com.github.tototoshi.csv._
import org.apache.spark.mllib.classification.LogisticRegressionWithSGD
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.tree.configuration.BoostingStrategy
import org.apache.spark.mllib.tree.{GradientBoostedTrees, DecisionTree}
import org.apache.spark.{SparkContext, SparkConf}
import org.apache.spark.mllib.linalg._
import org.apache.spark.mllib.stat.Statistics

object test {
  var minAge : Double = 0
  var maxAge : Double = 0
  var avgFare : Double = 0
  def main(args: Array[String]) {
    val conf = new SparkConf().setAppName("Simple Application")
    val sc = new SparkContext(conf)

    val train = CSVReader.open(new File("/home/nyaapa/Downloads/train.csv")).allWithHeaders()
    val headers = train(0).keys.toList

    setAgeGroups(train)
    avgFare = getAvgFare(train)

    val trainRdd = sc.makeRDD( train.map(row =>
      new LabeledPoint(row("Survived").toInt, features(row))
    ))

//
//    val boostingStrategy = BoostingStrategy.defaultParams("Classification")
//    boostingStrategy.setNumIterations(1000) // Note: Use more iterations in practice.
//    boostingStrategy.treeStrategy.setNumClasses(2)
//    boostingStrategy.treeStrategy.setMaxDepth(5)
//    boostingStrategy.treeStrategy.setMaxBins(32)
    //  Empty categoricalFeaturesInfo indicates all features are continuous.
    //boostingStrategy.treeStrategy.setCategoricalFeaturesInfo(Map[Int, Int]())

//    val model = GradientBoostedTrees.train(trainRdd, boostingStrategy);
    val model = LogisticRegressionWithSGD.train(trainRdd, 2);

    val precRdd = model.predict(sc.makeRDD( train.map(row => features(row) )))
      println("\nCoorelation is " + Statistics.corr(sc.makeRDD( train.map(row => row("Survived").toDouble)), precRdd))
//    val trainRdd = sc.makeRDD( train.map(row =>
//      new DenseVector({
//        val ageGroup = if ( row("Age") == "" )
//          1.0
//        else {
//          val age = row("Age").toDouble;
//          if ( age > minAge )
//            if ( age > maxAge )
//              2.0
//            else
//              1.0
//          else
//            0.0
//        }
//        Array(
//          if ( row("Survived") == "1" ) 1.0 else 0,
//          if ( row("Sex") == "female")  1.0 else 0,
//          row("Pclass").toInt,
//          ageGroup,
//          row("Fare").toDouble,
//          row("Fare").toDouble * ageGroup,
//          row("SibSp").toInt,
//          if ( row("Sex") == "female" || (row("SibSp").toInt > 0 && row("Pclass").toDouble < 2)) 1.0 else 0,
//          row("Parch").toInt
//        )
//      }).asInstanceOf[Vector]
//    ))
//    val correlMatrix: Matrix = Statistics.corr(trainRdd, "pearson")
//    val p = new java.io.PrintWriter(new File("/home/nyaapa/titanic/cout"))
//    correlMatrix.toArray.foreach(p.println)
//    correlMatrix.toArray.take(Math.sqrt(correlMatrix.toArray.size).toInt).foreach(println)

//    val men = train.filter(_("Sex") == "male")
//    println(s"men survived ${ men.filter(_("Survived") == "1").size * 1.0 / men.size  }")
//    val women = train.filter(_("Sex") == "female")
//    println(s"women survived ${ women.filter(_("Survived") == "1").size * 1.0 / women.size }")

//    val result = train
    val test = CSVReader.open(new File("/home/nyaapa/Downloads/test.csv")).allWithHeaders()

    setAgeGroups(test)
    avgFare = getAvgFare(test)
    val tprecRdd = model.predict(sc.makeRDD( test.map(row => features(row) )))
    val writer = CSVWriter.open(new File("/home/nyaapa/titanic/out"))
    writer.writeRow(List("PassengerId", "Survived"))
    test.zip(tprecRdd.collect()).foreach(row => writer.writeRow(List(row._1("PassengerId"), row._2.toString)))
  }

  def setAgeGroups(set : List[Map[String, String]]) {
    val els = set.map(_("Age")).filter(_ != "").map(_.toDouble)
    val min = els.min
    val max = els.max
    minAge = min + (max - min) / 3.0
    maxAge = min + (max - min) * 2.0 / 3.0
  }


  def getAvgFare(set : List[Map[String, String]]) = {
    val els = set.map(_("Fare")).filter(_ != "").map(_.toDouble)
    els.sum / els.size
  }


  def features(row: Map[String, String]) = new DenseVector({
    val ageGroup = if ( row("Age") == "" )
      1.0
    else
      if ( row("Age").toDouble > (minAge + maxAge) / 2.0 )
        0.0
      else
        1.0
    Array(
      if ( row("Sex") == "female" && row("Pclass").toInt < 3)  1.0 else 0,
      if ( row("Parch").toInt > 0 ) 1 else 0,
      ageGroup,
      row("Pclass").toInt,
      if ( row("Fare") == "" ) avgFare else row("Fare").toDouble,
      if ( row("SibSp").toInt > 0 ) 1 else 0
    )
  }).asInstanceOf[Vector]
}
